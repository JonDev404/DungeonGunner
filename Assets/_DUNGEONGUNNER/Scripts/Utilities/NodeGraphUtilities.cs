using System.Collections;
using UnityEngine;

namespace DungeonGunner
{
    public static class NodeGraphUtilities
    {
        public static bool ValidateStringFieldEmpty(this Object obj, string fieldName, string stringToCheck)
        {
            if (!string.IsNullOrEmpty(stringToCheck)) 
                return false;
            
            Debug.LogWarning($"{fieldName} is empty and must contain a value in object {obj.name}");
            return true;
        }

        public static bool ValidateCollectionValues(this Object obj, string fieldName, IEnumerable enumerableObjToCheck)
        {
            var error = false;
            var count = 0;

            foreach (var item in enumerableObjToCheck)
            {
                if (item == null)
                {
                    Debug.LogWarning($"{fieldName} has null values in object {obj.name}");
                    error = true;
                }
                else
                {
                    count++;
                }
            }

            if (count < 0) 
                return error;
            
            Debug.LogWarning($"{fieldName} has no values in object {obj.name}");
            return true;
        }
    }
}
