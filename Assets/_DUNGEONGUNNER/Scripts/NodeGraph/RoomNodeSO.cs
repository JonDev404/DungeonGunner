using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace DungeonGunner
{
    public class RoomNodeSO : ScriptableObject
    {
        [field: SerializeField] public RoomNodeTypeSO RoomNodeType { get; set; }
        
        public string Id { get; set; }
        public List<string> ParentRoomNodeIds { get; } = new();
        public List<string> ChildRoomNodeIds { get; } = new();
        public RoomNodeGraphSO RoomNodeGraph { get; set; }
        public RoomNodeTypeListSO RoomNodeTypes => RoomNodeTypeListSO.Get();
        
#if UNITY_EDITOR
        public Rect Rect { get; private set; }
        public bool IsLeftClickDragging { get; private set; }
        public bool IsSelected { get; set; }

        public void Init(Rect rect, RoomNodeGraphSO nodeGraph, RoomNodeTypeSO roomNodeType)
        {
            Rect = rect;
            Id = Guid.NewGuid().ToString();
            name = "RoomNode";
            RoomNodeGraph = nodeGraph;
            RoomNodeType = roomNodeType;
        }

        public void Draw(GUIStyle roomNodeStyle)
        {
            GUILayout.BeginArea(Rect, roomNodeStyle);
            EditorGUI.BeginChangeCheck();
            
            if (ParentRoomNodeIds.Count > 0 || RoomNodeType.IsEntrance)
                EditorGUILayout.LabelField(RoomNodeType.RoomNodeTypeName);
            else
            {
                var selected = RoomNodeTypes.Types.FindIndex(t => t == RoomNodeType);

                var selection = EditorGUILayout.Popup(string.Empty, selected, RoomNodeTypes.DisplayableTypes);
                RoomNodeType = RoomNodeTypes.Types[selection];

                if (RoomNodeTypes.Types[selected].IsCorridor && !RoomNodeTypes.Types[selection].IsCorridor ||
                    !RoomNodeTypes.Types[selected].IsCorridor && RoomNodeTypes.Types[selection].IsCorridor ||
                    !RoomNodeTypes.Types[selected].IsBossRoom && RoomNodeTypes.Types[selection].IsBossRoom)
                {
                    if (ChildRoomNodeIds.Count > 0)
                    {
                        for (var i = ChildRoomNodeIds.Count - 1; i >= 0; i--)
                        {
                            var childRoomNode = RoomNodeGraph.GetRoomNode(ChildRoomNodeIds[i]);

                            if (childRoomNode == null) 
                                continue;
                    
                            RemoveChildRoomNodeIDFromRoomNode(childRoomNode.Id);
                            childRoomNode.RemoveParentRoomNodeIDFromRoomNode(Id);
                        }
                    }
                }
            }
            
            if (EditorGUI.EndChangeCheck())
                EditorUtility.SetDirty(this);
            
            GUILayout.EndArea();
        }

        public bool AddChildRoomNodeIDToRoomNode(string childID)
        {
            if (!IsChildRoomValid(childID)) 
                return false;
            
            ChildRoomNodeIds.Add(childID);
            return true;
        }

        bool IsChildRoomValid(string childID)
        {
            var isConnectedBossNodeAlready = RoomNodeGraph.RoomNodes
                .Any(roomNode => roomNode.RoomNodeType.IsBossRoom && 
                                 roomNode.ParentRoomNodeIds.Count > 0);

            var roomNode = RoomNodeGraph.GetRoomNode(childID);

            return (!roomNode.RoomNodeType.IsBossRoom || !isConnectedBossNodeAlready) &&
                   !roomNode.RoomNodeType.IsNone &&
                   !ChildRoomNodeIds.Contains(childID) &&
                   Id != childID &&
                   !ParentRoomNodeIds.Contains(childID) &&
                   roomNode.ParentRoomNodeIds.Count <= 0 &&
                   (!roomNode.RoomNodeType.IsCorridor || !RoomNodeType.IsCorridor) &&
                   (roomNode.RoomNodeType.IsCorridor || RoomNodeType.IsCorridor) &&
                   (!roomNode.RoomNodeType.IsCorridor || ChildRoomNodeIds.Count < RoomNodesSettings.MAX_CHILD_CORRIDORS) &&
                   !roomNode.RoomNodeType.IsEntrance &&
                   (roomNode.RoomNodeType.IsCorridor || ChildRoomNodeIds.Count <= 0);
        }

        public bool AddParentRoomNodeIDToRoomNode(string parentID)
        {
            ParentRoomNodeIds.Add(parentID);
            return true;
        }

        public bool RemoveChildRoomNodeIDFromRoomNode(string childID)
        {
            return ChildRoomNodeIds.Remove(childID);
        }
        
        public bool RemoveParentRoomNodeIDFromRoomNode(string parentID)
        {
            return ParentRoomNodeIds.Remove(parentID);
        }

        public void ProcessEvents(Event currentEvent)
        {
            if (currentEvent.type == EventType.MouseDown)
                ProcessMouseDownEvent(currentEvent);
            else if (currentEvent.type == EventType.MouseUp)
                ProcessMouseUpEvent(currentEvent);
            else if (currentEvent.type == EventType.MouseDrag)
                ProcessMouseDragEvent(currentEvent);
        }

        void ProcessMouseDragEvent(Event currentEvent)
        {
            if (currentEvent.button == 0)
                ProcessLeftClickDrag(currentEvent);
        }

        void ProcessLeftClickDrag(Event currentEvent)
        {
            IsLeftClickDragging = true;
            DragNode(currentEvent.delta);
            GUI.changed = true;
        }

        public void DragNode(Vector2 delta)
        {
            var rect = Rect;
            rect.position += delta;
            Rect = rect;
            EditorUtility.SetDirty(this);
        }


        void ProcessMouseUpEvent(Event currentEvent)
        {
            if (currentEvent.button == 0)
                ProcessLeftClickUp();
        }

        void ProcessLeftClickUp()
        {
            IsLeftClickDragging = false;
        }

        void ProcessMouseDownEvent(Event currentEvent)
        {
            if (currentEvent.button == 0)
                ProcessLeftClickDown();
            else if (currentEvent.button == 1)
                ProcessRightClickDown(currentEvent);
        }

        void ProcessRightClickDown(Event currentEvent)
        {
            RoomNodeGraph.SetNodeToDrawConnectionLineFrom(this, currentEvent.mousePosition);
        }

        void ProcessLeftClickDown()
        {
            Selection.activeObject = this;

            IsSelected = !IsSelected;
        }
#endif
    }
}