using System;
using UnityEngine;

namespace DungeonGunner
{
    [CreateAssetMenu(fileName = nameof(RoomNodeTypeSO), menuName = "SOs/Dungeon/Room Node Type")]
    public class RoomNodeTypeSO : ScriptableObject
    {
        [field: SerializeField] public string RoomNodeTypeName { get; private set; }
        [field: SerializeField] public bool DisplayInNodeGraphEditor { get; private set; } = true;
        [field: SerializeField] public bool IsCorridor { get; private set; }
        [field: SerializeField] public bool IsCorridorNS { get; private set; }
        [field: SerializeField] public bool IsCorridorEW { get; private set; }
        [field: SerializeField] public bool IsEntrance { get; private set; }
        [field: SerializeField] public bool IsBossRoom { get; private set; }
        [field: SerializeField] public bool IsNone { get; private set; }
        
#if UNITY_EDITOR
        void OnValidate()
        {
            this.ValidateStringFieldEmpty(nameof(RoomNodeTypeName), RoomNodeTypeName);
        }
#endif
    }
}