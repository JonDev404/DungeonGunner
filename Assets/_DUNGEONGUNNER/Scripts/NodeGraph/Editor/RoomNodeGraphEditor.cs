using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

namespace DungeonGunner
{
    public class RoomNodeGraphEditor : EditorWindow
    {
        static RoomNodeGraphSO _currentRoomNodeGraph;
        static RoomNodeTypeListSO RoomNodeTypes => RoomNodeTypeListSO.Get();
        
        GUIStyle _roomNodeStyle;
        GUIStyle _roomNodeSelectedStyle;
        RoomNodeSO _currentRoomNode;

        Vector2 _graphOffset;
        Vector2 _graphDrag;

        const float NODE_WIDTH = 160f;
        const float NODE_HEIGHT = 75f;
        const int NODE_PADDING = 25;
        const int NODE_BORDER = 12;
        const float CONNECTING_LINE_WIDTH = 3f;
        const float CONNECTING_LINE_ARROW_SIZE = 6f;
        const float GRID_LARGE = 100f;
        const float GRID_SMALL = 25f;
        
        [MenuItem("Room Node Graph Editor", menuItem = "Window/Dungeon Editor/Room Node Graph Editor")]
        static void OpenWindow()
        {
            GetWindow<RoomNodeGraphEditor>("Room Node Graph Editor");
        }

        [OnOpenAsset(0)]
        static bool OnDoubleClickAsset(int instanceID, int line)
        {
            var roomNodeGraph = EditorUtility.InstanceIDToObject(instanceID) as RoomNodeGraphSO;

            if (roomNodeGraph == null)
                return false;
            
            OpenWindow();

            _currentRoomNodeGraph = roomNodeGraph;
            return true;
        }
        
        void OnEnable()
        {
            Selection.selectionChanged += HandleSelectionChanged;
            
            _roomNodeStyle = new GUIStyle
            {
                normal =
                {
                    background = EditorGUIUtility.Load("node1") as Texture2D,
                    textColor = Color.white
                },
                padding = new RectOffset(NODE_PADDING, NODE_PADDING, NODE_PADDING, NODE_PADDING),
                border = new RectOffset(NODE_BORDER, NODE_BORDER, NODE_BORDER, NODE_BORDER)
            };
            
            _roomNodeSelectedStyle = new GUIStyle
            {
                normal =
                {
                    background = EditorGUIUtility.Load("node1 on") as Texture2D,
                    textColor = Color.white
                },
                padding = new RectOffset(NODE_PADDING, NODE_PADDING, NODE_PADDING, NODE_PADDING),
                border = new RectOffset(NODE_BORDER, NODE_BORDER, NODE_BORDER, NODE_BORDER)
            };
        }

        void OnDisable()
        {
            Selection.selectionChanged -= HandleSelectionChanged;
        }

        void OnGUI()
        {
            if (_currentRoomNodeGraph != null)
            {
                DrawBackgroundGrid(GRID_SMALL, 0.2f, Color.gray);
                DrawBackgroundGrid(GRID_LARGE, 0.2f, Color.gray);
                DrawDraggedLine();
                ProcessEvents(Event.current);
                DrawRoomConnections();
                DrawRoomNodes();
            }
            
            if (GUI.changed)
                Repaint();
        }

        void DrawBackgroundGrid(float gridSize, float gridOpacity, Color gridColor)
        {
            var verticalLineCount = Mathf.CeilToInt((position.width + gridSize) / gridSize);
            var horizontalLineCount = Mathf.CeilToInt((position.height + gridSize) / gridSize);

            Handles.color = new Color(gridColor.r, gridColor.g, gridColor.b, gridOpacity);

            _graphOffset += _graphDrag * 0.5f;

            var gridOffset = new Vector3(_graphOffset.x % gridSize, _graphOffset.y % gridSize, 0);

            for (var i = 0; i < verticalLineCount; i++)
            {
                Handles.DrawLine(new Vector3(gridSize * i, -gridSize, 0) + gridOffset, 
                    new Vector3(gridSize * i, position.height + gridSize,  0f) + gridOffset);
            }
            
            for (var i = 0; i < horizontalLineCount; i++)
            {
                Handles.DrawLine(new Vector3(-gridSize, gridSize * i, 0) + gridOffset, 
                    new Vector3(position.width + gridSize, gridSize * i, 0f) + gridOffset);
            }

            Handles.color = Color.white;
        }

        void HandleSelectionChanged()
        {
            var roomNodeGraph = Selection.activeObject as RoomNodeGraphSO;

            if (roomNodeGraph == null)
                return;

            _currentRoomNodeGraph = roomNodeGraph;
            GUI.changed = true;
        }

        void DrawRoomConnections()
        {
            foreach (var roomNode in _currentRoomNodeGraph.RoomNodes)
            {
                if (roomNode.ChildRoomNodeIds.Count <= 0)
                    continue;

                foreach (var childRoomNodeID in roomNode.ChildRoomNodeIds)
                {
                    if (!_currentRoomNodeGraph.RoomNodeDict.TryGetValue(childRoomNodeID, out var childRoomNode))
                        continue;
                    
                    DrawConnectionLine(roomNode, childRoomNode);
                    GUI.changed = true;
                }
            }
        }

        void DrawConnectionLine(RoomNodeSO parentRoomNode, RoomNodeSO childRoomNode)
        {
            var startPos = parentRoomNode.Rect.center;
            var endPos = childRoomNode.Rect.center;

            var midPos = (endPos + startPos) / 2f;
            var direction = endPos - startPos;

            var arrowTailPoint1 =
                midPos - new Vector2(-direction.y, direction.x).normalized * CONNECTING_LINE_ARROW_SIZE;
            var arrowTailPoint2 =
                midPos + new Vector2(-direction.y, direction.x).normalized * CONNECTING_LINE_ARROW_SIZE;

            var arrowHeadPoint = midPos + direction.normalized * CONNECTING_LINE_ARROW_SIZE;
            
            Handles.DrawBezier(arrowHeadPoint, arrowTailPoint1, arrowHeadPoint, 
                arrowTailPoint1, Color.white, null, CONNECTING_LINE_WIDTH);
            
            Handles.DrawBezier(arrowHeadPoint, arrowTailPoint2, arrowHeadPoint, 
                arrowTailPoint2, Color.white, null, CONNECTING_LINE_WIDTH);
            
            
            Handles.DrawBezier(startPos, endPos, startPos, endPos, Color.white, null, CONNECTING_LINE_WIDTH);
            GUI.changed = true;
        }

        void DrawDraggedLine()
        {
            if (_currentRoomNodeGraph.LinePosition == Vector2.zero)
                return;

            var startPositionRect = _currentRoomNodeGraph.RoomNodeToDrawLineFrom.Rect;
            
            Handles.DrawBezier(
                startPositionRect.center, _currentRoomNodeGraph.LinePosition,
                startPositionRect.center, _currentRoomNodeGraph.LinePosition,
                Color.white, null, CONNECTING_LINE_WIDTH
                );
        }

        void DrawRoomNodes()
        {
            foreach (var roomNode in _currentRoomNodeGraph.RoomNodes)
                roomNode.Draw(roomNode.IsSelected ? _roomNodeSelectedStyle : _roomNodeStyle);

            GUI.changed = true;
        }

        void ProcessEvents(Event currentEvent)
        {
            _graphDrag = Vector2.zero;

            if (_currentRoomNode == null || !_currentRoomNode.IsLeftClickDragging)
                _currentRoomNode = GetRoomNode(currentEvent);

            if (_currentRoomNode == null || _currentRoomNodeGraph.RoomNodeToDrawLineFrom != null)
                ProcessRoomNodeGraphEvents(currentEvent);
            else
                _currentRoomNode.ProcessEvents(currentEvent);
        }

        RoomNodeSO GetRoomNode(Event currentEvent) =>
            _currentRoomNodeGraph.RoomNodes.FirstOrDefault(roomNode =>
                roomNode.Rect.Contains(currentEvent.mousePosition));

        void ProcessRoomNodeGraphEvents(Event currentEvent)
        {
            if (currentEvent.type == EventType.MouseDown)
                ProcessMouseDown(currentEvent);
            else if (currentEvent.type == EventType.MouseDrag)
                ProcessMouseDrag(currentEvent);
            else if (currentEvent.type == EventType.MouseUp)
                ProcessMouseUp(currentEvent);
        }

        void ProcessMouseUp(Event currentEvent)
        {
            if (currentEvent.button != 1 || _currentRoomNodeGraph.RoomNodeToDrawLineFrom == null) 
                return;
            
            var roomNode = GetRoomNode(currentEvent);

            if (roomNode != null && _currentRoomNodeGraph.RoomNodeToDrawLineFrom.AddChildRoomNodeIDToRoomNode(roomNode.Id))
            {
                roomNode.AddParentRoomNodeIDToRoomNode(_currentRoomNodeGraph.RoomNodeToDrawLineFrom.Id);
            }
                
            ClearLineDrag();
        }

        void ClearLineDrag()
        {
            _currentRoomNodeGraph.SetNodeToDrawConnectionLineFrom(null, Vector2.zero);
            GUI.changed = true;
        }

        void ProcessMouseDrag(Event currentEvent)
        {
            if (currentEvent.button == 1)
                ProcessRightMouseDrag(currentEvent);
            else if (currentEvent.button == 0)
                ProcessLeftMouseDrag(currentEvent.delta);
        }

        void ProcessLeftMouseDrag(Vector2 dragDelta)
        {
            _graphDrag = dragDelta;
            _currentRoomNodeGraph.RoomNodes.ForEach(roomNode =>
            {
                roomNode.DragNode(dragDelta);
            });

            GUI.changed = true;
        }

        void ProcessRightMouseDrag(Event currentEvent)
        {
            if (_currentRoomNodeGraph.RoomNodeToDrawLineFrom != null)
            {
                DragConnectingLine(currentEvent.delta);
                GUI.changed = true;
            }
        }

        void DragConnectingLine(Vector2 delta)
        {
            _currentRoomNodeGraph.LinePosition += delta;
        }

        void ProcessMouseDown(Event currentEvent)
        {
            if (currentEvent.button == 1)
            {
                ShowContextMenu(currentEvent.mousePosition);
            }
            else if (currentEvent.button == 0)
            {
                ClearLineDrag();
                ClearAllSelectedRoomNodes();
            }
        }

        void ClearAllSelectedRoomNodes()
        {
            _currentRoomNodeGraph.RoomNodes.ForEach(roomNode => roomNode.IsSelected = false);
            GUI.changed = true;
        }

        void ShowContextMenu(Vector2 mousePosition)
        {
            var menu = new GenericMenu();
            
            menu.AddItem(new GUIContent("Create Room Node"), false, CreateRoomNode, mousePosition);
            menu.AddSeparator("");
            menu.AddItem(new GUIContent("Select All Room Nodes"), false, SelectAllRoomNodes);
            menu.AddSeparator("");
            menu.AddItem(new GUIContent("Delete Selected Room Node Links"), false, DeleteSelectedRoomNodeLinks);
            menu.AddItem(new GUIContent("Delete Selected Room Nodes"), false, DeleteSelectedRoomNodes);
            
            menu.ShowAsContext();
        }

        void DeleteSelectedRoomNodes()
        {
            var roomNodesToDelete = new Queue<RoomNodeSO>();
            
            _currentRoomNodeGraph.RoomNodes.ForEach(roomNode =>
            {
                if (!roomNode.IsSelected || roomNode.RoomNodeType.IsEntrance) 
                    return;
                
                roomNodesToDelete.Enqueue(roomNode);
                    
                roomNode.ChildRoomNodeIds.ForEach(childRoomNodeId =>
                {
                    var childRoomNode = _currentRoomNodeGraph.GetRoomNode(childRoomNodeId);

                    if (childRoomNode != null)
                    {
                        childRoomNode.RemoveParentRoomNodeIDFromRoomNode(roomNode.Id);
                    }
                });
                
                roomNode.ParentRoomNodeIds.ForEach(parentRoomNodeId =>
                {
                    var parentRoomNode = _currentRoomNodeGraph.GetRoomNode(parentRoomNodeId);

                    if (parentRoomNode != null)
                    {
                        parentRoomNode.RemoveChildRoomNodeIDFromRoomNode(roomNode.Id);
                    }
                });
            });

            while (roomNodesToDelete.Count > 0)
            {
                var roomNodeToDelete = roomNodesToDelete.Dequeue();

                _currentRoomNodeGraph.RoomNodeDict.Remove(roomNodeToDelete.Id);
                _currentRoomNodeGraph.RoomNodes.Remove(roomNodeToDelete);

                DestroyImmediate(roomNodeToDelete, true);
                AssetDatabase.SaveAssets();
            }
        }

        void DeleteSelectedRoomNodeLinks()
        {
            _currentRoomNodeGraph.RoomNodes.ForEach(roomNode =>
            {
                if (!roomNode.IsSelected || roomNode.ChildRoomNodeIds.Count <= 0) 
                    return;
                
                for (var i = roomNode.ChildRoomNodeIds.Count - 1; i >= 0; i--)
                {
                    var childRoomNode = _currentRoomNodeGraph.GetRoomNode(roomNode.ChildRoomNodeIds[i]);

                    if (childRoomNode == null || !childRoomNode.IsSelected) 
                        continue;
                    
                    roomNode.RemoveChildRoomNodeIDFromRoomNode(childRoomNode.Id);
                    childRoomNode.RemoveParentRoomNodeIDFromRoomNode(roomNode.Id);
                }
            });
            
            ClearAllSelectedRoomNodes();
        }

        void SelectAllRoomNodes()
        {
            _currentRoomNodeGraph.RoomNodes.ForEach(roomNode => roomNode.IsSelected = true);
            GUI.changed = true;
        }

        void CreateRoomNode(object mousePosition)
        {
            if (_currentRoomNodeGraph.RoomNodes.Count == 0)
                CreateRoomNode(new Vector2(200f, 200f), RoomNodeTypes.Types.Find(t => t.IsEntrance));
            
            CreateRoomNode(mousePosition, RoomNodeTypes.Types.Find(t => t.IsNone));
        }
        
        void CreateRoomNode(object mousePositionObject, RoomNodeTypeSO roomNodeType)
        {
            var mousePosition = (Vector2)mousePositionObject;

            var roomNode = CreateInstance<RoomNodeSO>();
            _currentRoomNodeGraph.RoomNodes.Add(roomNode);

            roomNode.Init(new Rect(mousePosition, new Vector2(NODE_WIDTH, NODE_HEIGHT)), _currentRoomNodeGraph,
                roomNodeType);

            AssetDatabase.AddObjectToAsset(roomNode, _currentRoomNodeGraph);
            AssetDatabase.SaveAssets();
            
            _currentRoomNodeGraph.OnValidate();
        }
    }
}
