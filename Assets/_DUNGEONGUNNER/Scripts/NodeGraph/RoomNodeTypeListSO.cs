using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DungeonGunner
{
    [CreateAssetMenu(fileName = nameof(RoomNodeTypeListSO), menuName = "SOs/Dungeon/Room Node Type List")]
    public class RoomNodeTypeListSO : ScriptableObject
    {
        static RoomNodeTypeListSO _instance;

        public static RoomNodeTypeListSO Get()
        {
            if (_instance == null)
                _instance = Resources.Load<RoomNodeTypeListSO>(nameof(RoomNodeTypeListSO));

            return _instance;
        }
        
        [field: SerializeField] public List<RoomNodeTypeSO> Types { get; private set; }

        public string[] DisplayableTypes => Types
            .Select(t => t.DisplayInNodeGraphEditor ? t.RoomNodeTypeName : string.Empty)
            .ToArray();
        
        
#if UNITY_EDITOR
        void OnValidate()
        {
            this.ValidateCollectionValues(nameof(Types), Types);
        }
#endif
    }
}