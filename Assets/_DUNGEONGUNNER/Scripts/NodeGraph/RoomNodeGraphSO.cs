using System;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonGunner
{
    [CreateAssetMenu(fileName = nameof(RoomNodeGraphSO), menuName = "SOs/Dungeon/Room Node Graph")]
    public class RoomNodeGraphSO : ScriptableObject
    {
        public RoomNodeTypeListSO RoomNodeTypes { get; set; }
        public List<RoomNodeSO> RoomNodes { get; } = new();
        public Dictionary<string, RoomNodeSO> RoomNodeDict { get; } = new();

        void Awake()
        {
            InitRoomNodeDict();
        }

        void InitRoomNodeDict()
        {
            RoomNodeDict.Clear();

            foreach (var node in RoomNodes)
                RoomNodeDict[node.Id] = node;
        }

#if UNITY_EDITOR
        public RoomNodeSO RoomNodeToDrawLineFrom { get; private set; }
        public Vector2 LinePosition { get; set; }

        public void OnValidate()
        {
            InitRoomNodeDict();
        }

        public void SetNodeToDrawConnectionLineFrom(RoomNodeSO node, Vector2 position)
        {
            RoomNodeToDrawLineFrom = node;
            LinePosition = position;
        }

        public RoomNodeSO GetRoomNode(string roomNodeID)
        {
            return RoomNodeDict.TryGetValue(roomNodeID, out var roomNode) ? roomNode : null;
        }
#endif
    }
}
