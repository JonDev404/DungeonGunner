using System;

namespace DungeonGunner
{
    public struct ServiceInfo
    {
        public Type Type { get; }
        public IService Service { get; }

        public ServiceInfo(Type type, IService service)
        {
            Type = type;
            Service = service;
        }
    }
}
