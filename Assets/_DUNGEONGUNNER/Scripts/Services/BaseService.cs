using UnityEngine;

namespace DungeonGunner
{
    public abstract class BaseService : MonoBehaviour
    {
        public abstract ServiceInfo Init();
    }
}
