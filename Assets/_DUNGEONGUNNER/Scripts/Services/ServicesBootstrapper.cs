using UnityEngine;

namespace DungeonGunner
{
    public static class ServicesBootstrapper
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void Execute() =>
            Object.DontDestroyOnLoad(Object.Instantiate(Resources.Load<Services>(nameof(Services))));
    }
}
