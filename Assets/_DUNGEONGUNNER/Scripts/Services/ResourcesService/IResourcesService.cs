namespace DungeonGunner
{
    public interface IResourcesService : IService
    {
        public RoomNodeTypeListSO RoomNodeTypes { get; }
    }
}
