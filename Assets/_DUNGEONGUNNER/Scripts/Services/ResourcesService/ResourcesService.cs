using UnityEngine;

namespace DungeonGunner
{
    public class ResourcesService : BaseService, IResourcesService
    {
        [field: SerializeField] public RoomNodeTypeListSO RoomNodeTypes { get; private set; }
        
        public override ServiceInfo Init() => new(typeof(IResourcesService), this);
    }
}
